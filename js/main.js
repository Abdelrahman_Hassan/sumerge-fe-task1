var maxId = 3;

function removeMovie(id){
    document.getElementById(id).remove();
}

var movieImage

function addMovie(){
    var movieName = document.getElementById("add-movie-name");
    var movieImage = document.getElementById("add-movie-image");
    var movieRating = document.getElementById("add-movie-rating");

    var tableRow = document.createElement('tr');
    tableRow.innerHTML = "<td>" + movieName.value +"</td>" + 
                        "<td><img src=\"images/" + movieImage.files[0].name + "\"></td>" + 
                        "<td>" + movieRating.value +"</td>" + 
                        "<td><button class=\"btn btn-danger\" onclick=\"removeMovie(" + (maxId+1) + ")\">Remove</button></td>";

    tableRow.setAttribute("id", maxId+1)

    document.getElementById("add-movie").before(tableRow);

    movieName.value = "";
    movieImage.value = "";
    movieRating.value = "";
    maxId++;
}
